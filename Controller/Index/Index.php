<?php
namespace Oliverbode\WishlistLink\Controller\Index;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Customer\Model\Session;

class Index extends \Magento\Framework\App\Action\Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var Session
     */
    protected $session;

    /**
     * Index constructor.
     *
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        Session $session,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->getSession = $session;
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * @return 
     */
    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();

        if ($this->getSession->isLoggedIn()) {
            $block = $resultPage->getLayout()
                    ->createBlock('Magento\Framework\View\Element\Template')
                    ->setTemplate('Oliverbode_WishlistLink::loggedin.phtml')
                    ->toHtml();
        }
        else {
            $block = $resultPage->getLayout()
                    ->createBlock('Magento\Framework\View\Element\Template')
                    ->setTemplate('Oliverbode_WishlistLink::loggedout.phtml')
                    ->toHtml();
        }
        $this->getResponse()->setBody($block);
    }
}
